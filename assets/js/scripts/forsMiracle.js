jQuery(document).ready(function($){
  var validNumber = { required: true, number: true };
  var validURL = { required: true, url: true };
  var msgNumber = { required: "<i class='fa fa-exclamation-triangle'><i>", number: "<i class='fa fa-list-ol'><i>" };
  var msgUrl = { required: "<i class='fa fa-exclamation-triangle'><i>", number: "<i class='fa fa-globe'><i>" };
  var form = $("#formDetalle");
  form.validate({
    errorPlacement: function errorPlacement(error, element) {
      element.before(error);
    },
    errorElement: "div",
    errorClass: "error-line",
    rules: {
      nombre: "required",
      apellido: "required",
      email: {
        required: true,
        email: true    
      },
      telefono: validNumber,
      perfilFacebook: "required",
      nombreFacePage: validURL,
      segFace: validNumber,
      perfilTwitter: validURL,
      segTwitter: validNumber,
      perfilInstagram: validURL,
      segInsta: validNumber,
      linkPub: validURL,
      linkPubliGus: validURL,
      previo: "required",
      exAlumno: "required",
      participate: "required",
      lider: "required",
    },
    messages: {
      nombre: "<i class='fa fa-exclamation-triangle'><i>",
      apellido: "<i class='fa fa-exclamation-triangle'><i>",
      email: msgNumber,
      telefono: {
        required: "<i class='fa fa-exclamation-triangle'><i>",
        number: "<i class='fa fa-phone'><i>"
      },
      perfilFacebook: "<i class='fa fa-exclamation-triangle'><i>",
      nombreFacePage: msgUrl,
      segFace: msgNumber,
      perfilTwitter: msgUrl,
      segTwitter: msgNumber,
      perfilInstagram: msgUrl,
      segInsta: msgNumber,
      linkPub: msgUrl,
      linkPubliGus: msgUrl,
      previo: "<i class='fa fa-exclamation-triangle'><i>",
      exAlumno: "<i class='fa fa-exclamation-triangle'><i>",
      participate: "<i class='fa fa-exclamation-triangle'><i>",
      lider: "<i class='fa fa-exclamation-triangle'><i>",
    }
  });
   form.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    labels: {
      finish: "Finalizar",
      next: "Continuar",
      previous: "Regresar",
    },
    saveState: true,
    enableContentCache: true,
    onStepChanging: function (event, currentIndex, newIndex) {
      form.validate().settings.ignore = ":disabled,:hidden";
      return form.valid();
    },
    onFinishing: function (event, currentIndex)
    {
      form.validate().settings.ignore = ":disabled";
      return form.valid();
    },
    onFinished: function (event, currentIndex) {
      TweenMax.to($('.overlayGenerico'), 0.1, { autoAlpha:1 });
      TweenMax.fromTo($('#auto-generico'), 0.6, { top: '-300%'}, { delay: 0.2, top: "0%", ease:Power3.easeInOut, easeParams:[1.1,0.7], force3D: true });
      $(this).steps('reset');
    }
  });
  $('#closeModal').click(function(e){
    e.preventDefault();
    TweenMax.to($overlay, 0.1, { delay: 0.55, autoAlpha: 0 });
    TweenMax.to($modalGen, 0.55, { top: '300%', ease:Power3.easeInOut, force3D: true });
  });
});