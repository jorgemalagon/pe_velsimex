        
          </div>
        </main><!-- end.Main -->

        <footer class="g-footer">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-md-4 col-lg-4">
                <div class="brandFooter">
                  <a href="<?php echo site_url(''); ?>">
                    <img src="<?php echo get_template_directory_uri(''); ?>assets/img/logos/miracle_logo-01.svg" alt="Miracle Mike">
                  </a>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="copyFooter">
                  <p>&reg; 2019 Miracle Mike <!--<a href="">¿Quién es Mike?</a>--> <!-- <a href="<?php echo site_url(''); ?>/preguntas-frecuentes">Faq</a> --> <a href="<?php echo site_url(''); ?>/aviso-de-privacidad">Aviso de Privacidad</a></p>
                </div>
              </div>

            </div>
          </div>
        </footer><!-- end.Footer -->

        <!-- Back to top button -->
        <a id="button"></a>

        <div class="overlayGenerico">
          <div id="auto-generico" class="modalAviso">
            <button id="closeModal" class="btnCloseModal"><i class="fa fa-times"></i></button>
            <div class="contentInfo">
              <h2>¡Tienes lo que necesitamos!</h2>
              <p>1Felicidades eres un gran candidato para formar parte de nuestros entrenaimientos.<br><br>Nos pondremos en contacto contigo la brevedad!</p>
            </div>
          </div>
        </div>
        
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/plugins/nprogress.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/vendor/popper.js/dist/umd/popper.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/vendor/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/vendor/greensock/dist/TweenMax.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/plugins/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/plugins/additional-methods.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/plugins/jquery.steps.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/plugins/wow.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/plugins/slick.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/scripts/headerFx.min.js?ver=<?php echo rand(); ?>"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/scripts/layoutScripts.min.js?ver=<?php echo rand(); ?>"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyArZzQ_GtWMpgPaKGbohmHjrJFwww6reDs"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/scripts/fxMain.js?ver=<?php echo rand(); ?>"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/scripts/forsMiracle.min.js?ver=<?php echo rand(); ?>"></script>

        <script>
          $('body').show();
          $('.version').text(NProgress.version);
          NProgress.start();
          setTimeout(function() {
            NProgress.done(); $('.fadeX').addClass('out');
          }, 1000);

          <?php if($_GET['sec'] == ''): ?>
            $(document).ready(function(){
              $("#videoHome").get(0).play();
            });
          <?php endif; ?>

          // $("#b-0").click(function() { NProgress.start(); });
          // $("#b-40").click(function() { NProgress.set(0.4); });
          // $("#b-inc").click(function() { NProgress.inc(); });
          // $("#b-100").click(function() { NProgress.done(); });
        </script>
    </body>
</html>