<?php
//Eliminar cuando se integre en FrameWork o CMS
require_once('functions.php');
?>
<!DOCTYPE html>
<html lang="es-MX">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Velsimex| <?php echo TituloSecciones(); ?></title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="<?php get_template_directory_uri(''); ?>humnas.txt">
        <meta name="robots" content="index, follow">
	      <link rel="alternate" hreflang="es-mx" href="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(''); ?>favicon.ico">
		    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(''); ?>favicon.ico">
		    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(''); ?>favicon.ico">

        <!-- S T Y L E S - G E N E R A L -->
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(''); ?>assets/vendor/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(''); ?>assets/css/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(''); ?>assets/css/vendor/animate.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(''); ?>assets/css/vendor/hover-min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(''); ?>assets/css/nprogress.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(''); ?>assets/css/vendor/jquery.steps.css">
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/vendor/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>assets/js/vendor/jquery.cookie.js"></script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>var siteURL = '<?php echo get_site_url(); ?>';</script>

    </head>
    <body class="<?php echo classPage(); ?>" style="display: none;">

      <div class="overProgress fadeX"></div>

        <div class="toolsHero">
          <ul>
            <li><a href="https://www.facebook.com/miraclemikeacademy/" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <!-- <li><a href="" target="_blank"><i class="fa fa-twitter"></i></a></li> -->
            <li><a href="https://www.instagram.com/miraclemike_pia/" target="_blank"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
    
        <header id="headerGeneral" class="g-header">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-6 col-md-2">
                <div class="brand">
                  <a href="<?php echo site_url(''); ?>">
                    <img src="<?php echo get_template_directory_uri(''); ?>assets/img/logos/miracle_logo-01.svg" alt="">
                    <h1 class="site-title">Miracle Mike</h1>
                  </a>
                </div>
              </div>
              <div class="col-md-7 d-none d-md-block">
                <div class="menuMain">
                  <nav class="menuDesk">
                    <ul id="md">
                      <li><a href="<?php echo site_url(''); ?>/#entrenamiento">Entrenamiento</a></li>
                      <li><a href="<?php echo site_url(''); ?>/#profesores">Profesores</a></li>
                      <li><a href="<?php echo site_url(''); ?>/#ubicacion">Ubicación</a></li>
                    </ul>
                  </nav>
                </div>
              </div>
              <div class="col-6 col-md-3">
                <div class="toolsHead">
                  <button type="button" id="openMenuMobile" class="menu-mobile-btn" onClick="menuMobile()" data-menu-expand="false">
                    <span></span>
                  </button>
                  <!-- <div class="conteBtnHead">
                    <a class="btnsGenericos ani-btn" href="<?php echo site_url(''); ?>/pasos">Únete ahora</a>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </header><!-- end.Header -->

        <!-- MenuMobile -->
        <div id="mm" class="cont-menu-mobile" data-menu-expand="false">
          <div id="contListMenu" class="mnuContMob">

          </div>
        </div>
        <!-- end.MenuMobile -->

        <main>
          <div id="primary" class="content-area">
        