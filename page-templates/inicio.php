              <article class="bg-gen">
                <section class="hero-sec">
                  <!-- <div class="contHero contHero_mobile">
                    <div class="container">
                      <div class="row justify-content-md-start">
                        <div class="col-md-10 col-lg-6">
                          <div class="infohero">
                            <div class="titleHeroHome">
                              <h2 class="wow fadeInDown" data-wow-delay="0.25s" data-wow-duration="0.25s">GIVE<br>YOURSELF.</h2>
                            </div>
                            <div class="titleHeroHome">
                              <h5 class="wow fadeInDown" data-wow-delay="0.50s" data-wow-duration="0.25s">Become an influencer.</h5>
                            </div>
                            <p class="wow fadeInDown" data-wow-delay="0.75s" data-wow-duration="0.25s">Todos tenemos algo que decir y que aportar, pero no todos saben cómo hacerlo adecuada y efectivamente. Sé el influencer que marque la diferencia y hazte notar en el competitivo mundo de las redes sociales, con el primer Entrenamiento especializado para influencers.</p>
                            <div class="formUnete wow fadeInDown" data-wow-delay="0.60s" data-wow-duration="0.25s">
                              <form metho="POST" name="frmHero" id="frmHero">
                                <input class="form-control" type="email" name="emailHero" id="emailHero" placeholder="tucorreo@sitio.com">
                                <button type="submit">Únete ahora</button>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> -->
                  <video id="videoHome" autoplay loop muted poster="<?php echo get_template_directory_uri(); ?>assets/img/bgs/bg_video_miracle.png">
                    <source src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/MIRACLE_MIKE_TESTIGO_MASTER_Low_2.mp4" type="video/mp4">
                  </video>
                  <a class="btnVideo" href="https://convocatoria.miraclemike.mx/" target="_blank"></a>
                  <a href="#entrenamiento" class="scrollNext"><i class="fa fa-angle-down"></i></a>
                  <div class="header-title" style="opacity: 0.0"></div>
                </section>

                <div class="socialMobileBlock">
                  <ul>
                    <li><a href="https://www.facebook.com/miraclemikeacademy/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.instagram.com/miraclemike_pia/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                  </ul>
                </div>

                <section id="entrenamiento" class="modulo-cinco cd-background-wrapper">
                  <div class="container posrRel">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="descGen">
                          <h2>Entrenamiento</h2>
                          <p>Somos el primer Instituto en América Latina que brinda capacitación especializada para obtener las herramientas necesarias y desarrollar métodos eficaces que te permitan explotar tu expertise <b>-cualquiera que sea-</b>, a través de las redes sociales.</p>
                        </div>
                        <!-- <div class="contBtnMain">
                          <a class="btnsGenericos ani-btn" href="<?php echo site_url(''); ?>/pasos">Únete ahora</a>
                        </div> -->
                      </div>
                    </div>
                  </div>
                  <figure class="cd-floating-background">
                    <img class="imgAbs imgAbs-1" src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/mm_01.png" alt="">
                    <img class="imgAbs imgAbs-2" src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/mm_03.png" alt="">
                    <img class="imgAbs imgAbs-3" src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/mm_02.png" alt="">
                  </figure>
                </section>

                <section id="profesores" class="modulo-tres">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="descGen">
                          <h2>Profesores</h2>
                          <p>Contamos con un grupo de expertos de primer nivel en redes sociales, marketing, imagen empresarial, producción, fotografía, storytelling, diseño, comunicación estratégica y reputación de marcas, monetización, administración, psicología y más.</p>
                        </div>
                      </div>
                      <div class="col-12 slideProfCnt">

                        <!-- end.SlideProf -->
                        <div class="slideProfesrores">

                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_liz.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>LIZ PÉREZ VAN KURCZYN</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Co fundadora de la Consultoría de Imagen Pública Vireka, es experta en Imagología, consultoría de imagen pública e imagen estética.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_adriana_ibarrola.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>ADRIANA IBARROLA</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Health coach, coach ejecutiva y coach de vida certificada con amplia experiencia en comunicación y persuasión estratégica a nivel de mercadotecnia y percepción de imagen.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_armando.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>ARMANDO SERNA</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Fotógrafo, pintor y emprendedor; Co fundador y CEO de la Agencia Marte, experto en marcas, estrategias publicitarias, diseño, marketing y consultoría de negocios.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_ramon.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>RAMÓN MURGUÍA</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Director Institucional de Mercadotecnia y Desarrollo de Nuevos Negocios en Aliat Universidades, es experto en mercadotecnia, administración y estrategia de negocios.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_iveth.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>IVETH LAGOS</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Directora de Comunicaciones para marcas como: nextel, Chanel, Ioewe y Fideicomiso Boletazo; es experta en finanzas y relaciones públicas.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_lastra.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>ALEJANDRO LASTRA</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Abogado y Maestro en Planeación Urbana y Políticas Públicas, es experto relaciones institucionales, urbanismos en prevención de delitos, innovación y modelos novedosos.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_marcelo.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>MARCELO TORRES LLAMAS</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Co fundador de la Agencia Marte, experto en comunicación política, ciencias políticas, marketing digital y economía conductual.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_adriana.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>ADRIANA GÓMEZ</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Doctora en psicología clínica con especialidad en Manejo de Crisis, terapia dialéctica del comportamiento (DBT), terapia enfocada a la transferencia (TFP), mindfulness, depresión, ansiedad y estrés post-traumático.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_sebastian.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>SÉBASTIEN GUERRA</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Fotógrafo y productor con amplia experiencia que ha trabajado en importantes campañas de marcas como: Mágnum, Hugo Boss, TOUS, XBox, Whirlpool, Sears, Nissan, Corona, Banamex y más.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_alex.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>AXEL RUIZ</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Experto en producción, fotografía fija, iluminación y Data.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_juanluis.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>JUAN LUIS R. PONS</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Consultor especializado en curaduría y diseño de experiencias, contenidos, medios y stryshowing. Fue editor en jefe de Chilango y conductor de televisión.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_vianei.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>VIANEY ESQUINCA</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Socia Directora de Cuadrante Estrategia y Comunicación, escritora, conductora y analista política.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <!-- <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_luz.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>LUZ GOVEA</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Mercadóloga especialista en Influencer Marketing, experta en desarrollo de estrategias de branded content y relaciones públicas. Fue la primera Influencer Hunter de Circus Marketing.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div> -->
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_sai.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>SAI SÁNCHEZ</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Directora de Influencia Digital y de Socialand México, es experta en relaciones públicas, manejo de crisis digitales y dinámicas con influencers, así como en creación de contenido a través de influencers.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_adrian.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>ADRÍAN GARCÍA BARRAGÁN</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Abogado especialista en derecho informático, propiedad intelectual, nuevas tecnologías y telecomunicaciones. Árbitro en procedimientos regulados por la Ley Federal del Derecho de Autor.</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div>
                          <!-- <div>
                            <figure class="boxProfesor">
                              <div class="circleImage">
                                <img src="<?php echo get_template_directory_uri(); ?>assets/img/imgPrueba/img_slider.jpg" alt="">
                              </div>
                              <figcaption>
                                <h5>FERNANDO GAY</h5>
                                <div class="puesto">
                                  <p><small>Miracle Mike</small></p>
                                </div>
                                <div class="descripcion">
                                  <p>Periodista, actor y conductor de televisión (TV Azteca y Fox Sports).</p>
                                </div>
                              </figcaption>
                            </figure>
                          </div> -->

                        </div>
                        <!-- end.SlideProf -->

                        <button type="button" id="prevslideHome" class="btnSlideProd bLeftP"></button>
                        <button type="button" id="nextslideHome" class="btnSlideProd nRightP"></button>

                      </div>
                      <div class="col-12">
                        <div class="contToolProf">
                          <!-- <a class="btnsGenericos ani-btn" href="<?php echo site_url(''); ?>/pasos">Únete ahora</a> -->
                          <!-- <div class="clearfix"></div>
                          <a class="viewText" href="">Ver más</a> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </section>

                <section id="ubicacion" class="modulo-cuatro">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-6 col-lg-7">
                        <div class="mapMiracle">
                          <!-- <div id="cd-google-map">
                            <div id="google-container"></div>
                            <div id="cd-zoom-in"></div>
                            <div id="cd-zoom-out"></div>
                          </div> -->
                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3762.6063674338498!2d-99.20990228538903!3d19.429406486886077!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d201f5f0c70a61%3A0xe269bd0a85cd3989!2s1er.%20Piso%2C%20Volc%C3%A1n%20150%2C%20Lomas%20-%20Virreyes%2C%20Lomas%20de%20Chapultepec%2C%2011000%20Ciudad%20de%20M%C3%A9xico%2C%20CDMX!5e0!3m2!1ses!2smx!4v1574732038155!5m2!1ses!2smx" width="600" height="450" frameborder="0" style="border:0;display: block;" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-md-6 col-lg-5">
                        <div class="contUbicacion">
                          <figure class="logoUbi">
                            <img src="<?php echo get_template_directory_uri(''); ?>assets/img/logos/miracle_logo-01.svg" alt="Miracle Mike">
                          </figure>
                          <div class="descGen">
                            <!-- <h2>Ubicación</h2>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quisquam eius ducimus commodi, nemo quo ut corporis dolores est quia porro totam</p> -->
                            <address>Vólcan #150 1er. Piso, Lomas Virreyes,<br>Miguel Hidalgo, CDMX</address>
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod sequi illo odit magnam impedit sed. Velit aliquid voluptatibus pariatur. Laboriosam necessitatibus architecto possimus voluptatibus qui! Quisquam voluptas quam temporibus et!</p> -->
                          </div>
                          <!-- <div class="formUneteUbi wow fadeInDown" data-wow-delay="0.60s" data-wow-duration="0.25s">
                            <form metho="POST" name="frmHeroUbi" id="frmHeroUbi">
                              <input class="form-control" type="email" name="emailUbi" id="emailUbi" placeholder="tucorreo@sitio.com">
                              <button type="submit">Únete ahora</button>
                            </form>
                          </div> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                

              </article><!-- end.article inicio --> 